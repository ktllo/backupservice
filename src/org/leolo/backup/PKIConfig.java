package org.leolo.backup;


/**
 * This class provide setting for PKI systems
 * @author leolo
 *
 */
public class PKIConfig{
	
	public PKIConfig(String file, String passphase, String entry) {
		super();
		this.file = file;
		this.passphase = passphase;
		this.entry = entry;
	}
	private String file;
	private String passphase;
	private String entry;
	private FileType fileType = FileType.PKCS12;
	public enum FileType{
		PKCS12;
	}
	public String getFile() {
		return file;
	}
	public FileType getFileType() {
		return fileType;
	}
	public String getPassphase() {
		return passphase;
	}
	public String getEntry() {
		return entry;
	}
}