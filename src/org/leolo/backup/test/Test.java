package org.leolo.backup.test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.security.GeneralSecurityException;
import java.util.Random;

import org.apache.commons.codec.binary.Base64;
import org.leolo.backup.Configuration;
import org.leolo.backup.util.pki.PKISignedPackage;
import org.leolo.backup.util.pki.PKIWorker;

public class Test {
	public static void main(String [] args) throws Exception{
		Configuration conf = Configuration.getInstance();
		byte d [] = new byte [1048576];
		Random r = new Random();
		r.nextBytes(d);
		PKISignedPackage psp = PKIWorker.sign(d, "default");
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ObjectOutputStream oos = new ObjectOutputStream(baos);
		oos.writeObject(psp);
		byte [] da = baos.toByteArray();
//		System.out.write(Base64.encodeBase64(da, true));
		ObjectInputStream ois = new ObjectInputStream(
				new ByteArrayInputStream(Base64.decodeBase64(Base64.encodeBase64(da, true))));
		Object o =  ois.readObject();
		if(o instanceof PKISignedPackage){
			PKIWorker.verify((PKISignedPackage) o);
		}
	}
}
