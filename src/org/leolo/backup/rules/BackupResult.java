package org.leolo.backup.rules;

public enum BackupResult {
	
	INCLUDED(true, false),
	INCONCLISIVE(false, false),
	EXCLUDED(false, true);
	
	
	private BackupResult(boolean include, boolean exclude) {
		this.include = include;
		this.exclude = exclude;
	}
	private boolean include;
	private boolean exclude;
	public boolean isIncluded() {
		return include;
	}
	public boolean isExcluded() {
		return exclude;
	}
	
}
