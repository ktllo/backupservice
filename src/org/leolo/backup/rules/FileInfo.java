package org.leolo.backup.rules;

import java.util.Map;

public final class FileInfo {
	
	private String path;
	private long size;
	
	private boolean isHidden;
	
	
	private Map<String, String> metadata;
	
	public void addMetaData(String key, String data){
		metadata.put(key, data);
	}
	
	public String getMetaData(String key){
		return metadata.get(key);
	}
	
	class FilePermission{
		boolean readable;
		boolean writeable;
		boolean executable;
	}
	
	enum FileType{
		/** 
		 * Denotes a regular file , <i>i.e.</i>, not a directory, nor a special file.
		 * 
		 * The lnk file on Windows are consider as regular file, rather than {@link #SYMBOLIC_LINK symbolic links}
		 */
		REGULAR,
		/** Denotes a directory */
		DIRECTORY,
		/** 
		 * Denotes a symbolic link.
		 * 
		 *  The lnk files on Windows are consider as {@link #REGULAR} file
		 */
		SYMBOLIC_LINK,
		/**
		 * Denotes the file is other special file type, <i>e.g.</i> block device, <i>etc.</i>.
		 */
		OTHER;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public long getSize() {
		return size;
	}

	public void setSize(long size) {
		this.size = size;
	}

	public boolean isHidden() {
		return isHidden;
	}

	public void setHidden(boolean isHidden) {
		this.isHidden = isHidden;
	}
	
}
