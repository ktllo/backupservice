package org.leolo.backup.rules;

import java.nio.file.Path;
import java.util.List;
import java.util.Vector;

public class RuleSet extends BackupRule{
	
	List<BackupRule> rules = new Vector<>();
	
	@Override
	public BackupResult eveulate(Path p) {
		for(BackupRule rule:rules){
			BackupResult result = rule.eveulate(p);
			if(result != BackupResult.INCONCLISIVE)
				return result;
		}
		return BackupResult.INCONCLISIVE;
	}

}
