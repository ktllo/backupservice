package org.leolo.backup;

import java.util.List;

import org.apache.commons.collections4.list.UnmodifiableList;

public class Policy {
	private List<String> signature;
	
	public List<String> getSignatures(){
		return UnmodifiableList.unmodifiableList(signature);
	}
	
	void setSingatures(List<String> signature){
		this.signature = signature;
	}
}
