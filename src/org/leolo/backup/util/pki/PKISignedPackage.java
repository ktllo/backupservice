package org.leolo.backup.util.pki;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.security.GeneralSecurityException;
import java.security.cert.X509Certificate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PKISignedPackage implements java.io.Serializable{
	private byte [] signedData;
	private X509Certificate cert;
	private byte [] singature;
	private transient VerifyStatus status;
	public enum VerifyStatus{
		/**
		 * Indicate that the data have not be verified yet
		 */
		UNVERIFY,
		/**
		 * Indicate that the data is verified, however, the certificate chain cannot be, or
		 * have not be verified.
		 */
		PARTIAL,
		/**
		 * Indicate that the data and the certificate chain have been verified.
		 */
		FULL_CHAIN,
		/**
		 * Indicate that the data have been successfully verified, however, the certificate
		 * chain verification is failed. This means that the singer's identity cannot be
		 * established. 
		 */
		CHAIN_FAIL,
		/**
		 * Indicate that the verification against the data is failed. This usually means that
		 * the data, or the signature, is being tempered with.
		 */
		FAILED;
	}
	
	public byte[] getSignedData() {
		return signedData;
	}
	protected void setSignedData(byte[] signedData) {
		this.signedData = signedData;
		this.status = VerifyStatus.UNVERIFY;
	}
	public X509Certificate getCert() {
		return cert;
	}
	protected void setCert(X509Certificate x509Certificate) {
		this.cert = x509Certificate;
		this.status = VerifyStatus.UNVERIFY;
	}
	public byte[] getSingature() {
		return singature;
	}
	protected void setSingature(byte[] singature) {
		this.singature = singature;
		this.status = VerifyStatus.UNVERIFY;
	}
	
	public InputStream getInputStream()throws GeneralSecurityException{
		return getInputStream(false);
	}
	
	public InputStream getInputStream(boolean relaxMode) throws GeneralSecurityException{
		if(
				status != VerifyStatus.PARTIAL &&
				status != VerifyStatus.FULL_CHAIN &&
				status != VerifyStatus.CHAIN_FAIL &&
				!relaxMode
			){
			throw new GeneralSecurityException();
		}
		ByteArrayInputStream baos = new ByteArrayInputStream(signedData);
		return baos;
	}
	public VerifyStatus getStatus() {
		return status;
	}
	protected void setStatus(VerifyStatus status) {
		this.status = status;
	}
}
