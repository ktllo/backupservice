package org.leolo.backup.util.pki;

import java.security.PrivateKey;
import java.security.cert.X509Certificate;

public class Key {
	private X509Certificate cert;
	private transient PrivateKey key;
	public X509Certificate getCert() {
		return cert;
	}

	public void setCert(X509Certificate cert) {
		this.cert = cert;
	}

	public PrivateKey getKey() {
		return key;
	}

	public void setKey(PrivateKey key) {
		this.key = key;
	}
}
