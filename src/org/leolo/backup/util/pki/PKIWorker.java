package org.leolo.backup.util.pki;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.KeyFactory;
import java.security.KeyStoreException;
import java.security.PrivateKey;
import java.security.Security;
import java.security.Signature;
import java.security.SignatureException;
import java.security.cert.CertificateExpiredException;
import java.security.cert.CertificateNotYetValidException;
import java.security.cert.X509Certificate;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.DERBMPString;
import org.bouncycastle.asn1.pkcs.Attribute;
import org.bouncycastle.asn1.pkcs.ContentInfo;
import org.bouncycastle.asn1.pkcs.PKCSObjectIdentifiers;
import org.bouncycastle.asn1.pkcs.PrivateKeyInfo;
import org.bouncycastle.cert.X509CertificateHolder;
import org.bouncycastle.cert.jcajce.JcaX509CertificateConverter;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.operator.InputDecryptorProvider;
import org.bouncycastle.operator.bc.BcDefaultDigestProvider;
import org.bouncycastle.pkcs.PKCS12PfxPdu;
import org.bouncycastle.pkcs.PKCS12SafeBag;
import org.bouncycastle.pkcs.PKCS12SafeBagFactory;
import org.bouncycastle.pkcs.PKCS8EncryptedPrivateKeyInfo;
import org.bouncycastle.pkcs.PKCSException;
import org.bouncycastle.pkcs.bc.BcPKCS12MacCalculatorBuilderProvider;
import org.bouncycastle.pkcs.jcajce.JcePKCSPBEInputDecryptorProviderBuilder;
import org.bouncycastle.util.io.Streams;
import org.leolo.backup.Configuration;
import org.leolo.backup.PKIConfig;
import org.leolo.backup.util.pki.PKISignedPackage.VerifyStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PKIWorker {
	
	private static Logger logger = LoggerFactory.getLogger(PKIWorker.class);
	public static PKISignedPackage sign(byte[] data, String keyName) throws GeneralSecurityException, IOException{
		Key key = readPKCS12(keyName);
		logger.info("Signing data with {}"
				, key.getCert().getSubjectDN());
		Signature signature = Signature.getInstance(Configuration.getInstance().getPolicy().getSignatures().get(0), "BC");
        signature.initSign(key.getKey());
        signature.update(data);
        PKISignedPackage psp = new PKISignedPackage();
        psp.setSignedData(data);
        psp.setCert(key.getCert());
        psp.setSingature(signature.sign());
        psp.setStatus(PKISignedPackage.VerifyStatus.PARTIAL);
		return psp;
	}
	
	public static void verify(PKISignedPackage psp) throws GeneralSecurityException{
		Signature signature = Signature.getInstance(Configuration.getInstance().getPolicy().getSignatures().get(0), "BC");
        signature.initVerify(psp.getCert());
        signature.update(psp.getSignedData());
        if(signature.verify(psp.getSingature())){
        	logger.info("Verified signature from {}", psp.getCert().getSubjectDN());
        	psp.setStatus(VerifyStatus.PARTIAL);
        }else{
        	logger.info("Cannot verify signature from {}", psp.getCert().getSubjectDN());
        	psp.setStatus(VerifyStatus.FAILED);
        }
	}
	
	private static Key readPKCS12(String keyName) throws GeneralSecurityException, IOException{
		PKIConfig key = Configuration.getInstance().getPkiConfig(keyName);
		if(key==null){
			logger.error("Key named {} is not found. Please check the config file.", keyName);
			throw new GeneralSecurityException("Key not found");
		}
		Security.addProvider(new BouncyCastleProvider());
		PKCS12PfxPdu pfx = null;
		try {
			pfx = new PKCS12PfxPdu(Streams.readAll(new FileInputStream(key.getFile())));
		} catch (IOException e) {
			throw e;
		}
		Key k = new Key();
		try {
			if (!pfx.isMacValid(new BcPKCS12MacCalculatorBuilderProvider(BcDefaultDigestProvider.INSTANCE), key.getPassphase().toCharArray())){
				throw new GeneralSecurityException("PKCS#12 MAC test failed");
			}
		
			ContentInfo[] infos = pfx.getContentInfos();
			InputDecryptorProvider inputDecryptorProvider = new JcePKCSPBEInputDecryptorProviderBuilder()
	                .setProvider("BC").build(key.getPassphase().toCharArray());
			JcaX509CertificateConverter jcaConverter = new JcaX509CertificateConverter().setProvider("BC");
			for(ContentInfo ci:infos){
				if(ci.getContentType().equals(PKCSObjectIdentifiers.encryptedData)){
					PKCS12SafeBagFactory dataFact = new PKCS12SafeBagFactory(ci, inputDecryptorProvider);
	
					PKCS12SafeBag[] bags = dataFact.getSafeBags();
bag: 				for(PKCS12SafeBag bag:bags){
						X509CertificateHolder certHldr = (X509CertificateHolder)bag.getBagValue();
						X509Certificate cert = jcaConverter.getCertificate(certHldr);
						for(Attribute attr:bag.getAttributes()){
							if(attr.getAttrType().equals(PKCS12SafeBag.friendlyNameAttribute)){
								if(key.getEntry().equals(((DERBMPString)attr.getAttributeValues()[0]).getString())){
									try{
										cert.checkValidity();
									}catch(CertificateExpiredException| CertificateNotYetValidException e){
										logger.info("Certificate is invalid.");
										continue bag;
									}
									logger.info("Certificate {} is found for entry {}", cert.getSubjectDN(), key.getEntry());
									k.setCert(cert);
								}
							}
						}
					}
				}else{
					PKCS12SafeBagFactory dataFact = new PKCS12SafeBagFactory(ci);
					
					PKCS12SafeBag bag = dataFact.getSafeBags()[0];
							
					PKCS8EncryptedPrivateKeyInfo encInfo = (PKCS8EncryptedPrivateKeyInfo)bag.getBagValue();
	                PrivateKeyInfo info = encInfo.decryptPrivateKeyInfo(inputDecryptorProvider);

	                KeyFactory keyFact = KeyFactory .getInstance(info.getPrivateKeyAlgorithm().getAlgorithm().getId(), "BC");
	                PrivateKey privKey = keyFact.generatePrivate(new PKCS8EncodedKeySpec(info.getEncoded()));
	                for(Attribute attr:bag.getAttributes()){
						if(attr.getAttrType().equals(PKCS12SafeBag.friendlyNameAttribute)){
							if(key.getEntry().equals(((DERBMPString)attr.getAttributeValues()[0]).getString())){
								logger.info("Key found for entry {}", key.getEntry());
								k.setKey(privKey);
							}
						}
					}
	                
				}
			}
		} catch (PKCSException e) {
			throw new GeneralSecurityException(e.getMessage(),e);
		}
		if(k.getCert()==null||k.getKey()==null){
			throw new KeyStoreException("Cannot found the signing key");
		}
		return k;
	}
}
