package org.leolo.backup;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class Configuration {
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	private static Configuration instance = null;
	private Document doc;
	
	private Map<String,PKIConfig> pkiConfig;
	private Policy policy;
	public static synchronized Configuration getInstance(){
		if(instance==null){
			instance = new Configuration();
		}
		return instance;
	}
	
	private Configuration(){
		loadConfiguration();
	}
	
	private synchronized void loadConfiguration(){
		try {
			File fXmlFile = new File("config.xml");
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder;
		
			dBuilder = dbFactory.newDocumentBuilder();
			doc = dBuilder.parse(fXmlFile);
			
			
			if(!"Configuration".equals(doc.getDocumentElement().getNodeName())){
				logger.error("Illegal root element. Configuration expected, {} found", doc.getDocumentElement().getNodeName());
				Runtime.getRuntime().halt(Helper.ERROR_FATAL_LOADING);
			}
			//optional, but recommended
			//read this - http://stackoverflow.com/questions/13786607/normalization-in-dom-parsing-with-java-how-does-it-work
			doc.getDocumentElement().normalize();
			
			pkiConfig = loadPKIConfig();
			loadPolicy();
		} catch (ParserConfigurationException | SAXException | IOException e) {
			logger.error(e.getMessage(), e);
			Runtime.getRuntime().halt(Helper.ERROR_FATAL_LOADING);
		}
		
				
		
	}
	
	private Map<String,PKIConfig> loadPKIConfig(){
		NodeList pkiNodeList = doc.getElementsByTagName("pki");
		if(pkiNodeList.getLength()==0){
			logger.warn("No PKI information given. Will not able to perform PKCS#7 signing");
			return null;
		}else if(pkiNodeList.getLength()>1){
			logger.warn("More than 1 PKI setting section is found!");
		}
		Map<String,PKIConfig> result = new Hashtable<>();
		for(int i=0;i<pkiNodeList.getLength();i++){
			Node pkiNode = pkiNodeList.item(i);
			NodeList entryList = pkiNode.getChildNodes();
			for(int j=0;j<entryList.getLength();j++){
				Node entry = entryList.item(j);
				if(entry.getNodeType()==Node.ELEMENT_NODE && "pkcs12".equals(entry.getNodeName())){
					//Process a node
					String file = getAttribute(entry,"file");
					String name = getAttribute(entry,"name");
					String passphase = getAttribute(entry,"passphase");
					String entryName = getAttribute(entry, "entry");
					if(file==null||passphase==null||entryName==null){
						logger.error("File name, entry name and passphase are required.");
						continue;
					}
					if(name==null){
						name = Integer.toHexString(file.concat(passphase).hashCode());
						while(name.length()<8) name = "0"+name;
						logger.warn("No name were given for file {}. {} has been set as name.", file, name);
					}
					if(result.containsKey(name)){
						logger.error("Duplicated name {}. Only first one is used.", name);
					}else{
						PKIConfig pkic = new PKIConfig(file, passphase, entryName);
						result.put(name, pkic);
					}
				}
			}
		}
		logger.info("{} PKI info were loaded.", result.size());
		return result;
	}
	
	private void loadPolicy(){
		//TODO: Check is another policy file was specified
		ClassLoader classLoader = getClass().getClassLoader();
		File file;
		try {
			file = new File(URLDecoder.decode(classLoader.getResource("policy.xml").getFile(),"UTF-8"));
			loadPolicy(file);
		}catch(IOException e){
			logger.error(e.getMessage(), e);
		}
	}
	
	private void loadPolicy(File f) throws IOException{
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder;
		Policy p  = new Policy();
		try {
			dBuilder = dbFactory.newDocumentBuilder();
			doc = dBuilder.parse(f);
			doc.getDocumentElement().normalize();
			//Step 1: Load Signatures
			NodeList signatureNodeList = doc.getElementsByTagName("Signature");
			if(signatureNodeList.getLength()==0){
				logger.warn("No signature list given");
			}else if(signatureNodeList.getLength()>1){
				logger.warn("More than 1 signature setting section is found! Only the first one is processed");
			}
			p.setSingatures(getAlgorithms(signatureNodeList.item(0)));
		} catch (ParserConfigurationException | SAXException e) {
			logger.error(e.getMessage(), e);
		}
		
		policy = p;
	}
	
	private List<String> getAlgorithms(Node n){
		if(!(n instanceof Element)){
			return null;
		}
		Element e = (Element) n;
		NodeList nl = e.getElementsByTagName("algorithm");
		Vector<String> v = new Vector<>(nl.getLength());
		for(int i=0;i<nl.getLength();i++){
			v.addElement(getAttribute(nl.item(i),"name"));
		}
		logger.debug("Algo list is {}", v);
		return v;
	}
	
	private String getAttribute(Node node, String AttributeName){
		Node attr = node.getAttributes().getNamedItem(AttributeName);
		return attr==null?null:attr.getNodeValue();
	}
	public PKIConfig getPkiConfig(String name) {
		return pkiConfig.get(name);
	}
	
	public Policy getPolicy(){
		return policy;
	}
}